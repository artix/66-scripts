SCRIPT_TARGET := \
module/boot@/configure/crypt.awk \
module/boot@/configure/modules.sh \
module/boot@/configure/binfmt.sh

SERVICE_TARGET := service/boot@
MODULE_TARGET := $(shell find module/boot@/service -type f)
MODULE_INSTANCE_TARGET := $(shell find module/boot@/service@ -type f)

MODULE_CONFIGURE_TARGET := module/boot@/configure/configure

SKEL_SCRIPT_TARGET := module/boot@/configure/rc.local
