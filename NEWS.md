# Changelog for boot-66serv

---

# In 2.4.0

- Behavior changes:
    - local-{iptables,ip6tables,ebtables,nftables,arptables}: they use now the standard log location instead of the uncaught-logs.
    - devices-zfs: use `-a -l` options by default and a `zfs share -a` command was added.
    - devices-crypttab: use `-a ay` as options by default. Remove devices-brtfs dependency.
    - devices-lvm: use `-a ay` as options by default.
    - mount-swap: do not crash the boot sequence but warn the user instead if the swap cannot be mounted.

---

# In 2.3.1

- Behavior changes:
    - devices-crypttab:
        - add dependencies of all devices-xxx services.
        - do not block if lvm crash and warn user.
    - local-iptables: stop script is now hardcoded instead of calling a script.
    - mount-swap: do not crash if swapon command fail and warn the user.
    - local-tmpfiles: do not crash if a files have bad format and warn the user.
    - configure script: use zpool cache by default for ZFS_IMPORT variable.

- Bug fix:
    - local-iptables: fix quotation with the call of the 66-yeller program.
    - local-dmesg: fix default options.
    - configure script: fix typo of the devices-brtfs name.

---

# In 2.3.0

- Adapt to execline 2.7.0.0
- Adapt to s6 2.10.0.0
- Adapt to s6-rc 0.5.2.1
- Adapt to 66 0.6.0.0
- Adapt to 66-tools 0.0.7.0
- Adapt to s6-linux-utils 2.5.1.4
- Adapt to s6-portable-utils 2.2.3.1

---

# In 2.2.1

- Convenient release to update dependencies:
    - 66 version v0.5.1.0
    - 66-tools version v0.0.6.2
    - tty@-66serv version v0.2.0

- Documentation fix and update

---

# In 2.2.0

- Bufs fix:
    - Remove the check and creation of `/sys/kernel/debug/tracing` mountpoint. This is done previously by the check and creation of `/sys/kernel/debug/` mountpoint.
    - *local-authfiles*:
        - fix the name of the variable to use.

- Add documentation.
    - The documentation can be installed at compile time with the flags `install-html` and `install-man`.

- *mount-tmp*, *mount-pts*, *mount-shm*:
    - Add lazy umount at `@execute` field of `[stop]` section.

- *tmpfiles.sh*:
    - update from upstream

- New variable `FIREWALL`:
    - This variable replace the IPTABLES and IP6TABLES variables. It allow to choose between *iptables*, *ip6tables*, *nftables*, *ebtables* and *arptables* program to set a firewall. Refer to the documentation for further information. For example:
        ```
        FIREWALL=!nftables
        ```

- New services:
    - local-nftables, local-ebtables and local-arptables to handle firewall.

---

# In 2.1.0

- Adapt to oblibs 0.0.9.0, 66 0.4.0.0 and 66-tools 0.0.6.0
- Bugs fix:
    - ZFS_IMPORT valid value
    - regex of the LIVE variable at all-Runtime in case of crash
    - retrieve all behavior of the system-random service.

- *all-Runtime* services do not crash anymore if a runtime service fails to start. It only warns the user about the state.
- Symlink previously at system_administration/ (e.g. /etc/66) is no longer created.
- *Udevd* has its own logger created at the livedir/log/udevd (e.g. /run/66/log/udevd).
- *configure* script now respects the choice about the color set at *66-enable*.
- According to the 66 >v0.4.0.0, all @build fields were removed. These fields are no longer mandatory.
- Reorganize the environment configuration file to be clearer as much as possible for lambda user.

---

# In 2.0.0

- Pass to module type.
- A lot of flags were added. Please see `configure --help` for further details which are self-explanatory.

---

# In 1.2.1

- Bugs and typo fix

---

# In 1.2.0

- Bugs fix: tmpfiles.sh
- Modules.sh is now pure sh
- Fix swap file: active swap after remounting / rw
- Full support of encryption password using 66-olexec

---

# In 1.1.2

- Bugs fix: fix devices-zfs makefile variable

---

# In 1.1.1

- Pass zfs to system-Devices bundle
- Wait for all-Mount at system-fsck
- Mount cgroups before run udevd

---

# In 1.1.0

- Fix mount of cgroups and zfs
- Update tmpfiles.sh script
- Add local-sethostname service: it overwrite /etc/hostname file with the value of HOSTNAME from boot.conf
- Ported to 66 version 0.2.1.0: remove @name field

---

# In 1.0.1

- Fix permissions of /tmp/.X11-unix and /tmp/.ICE-unix to 1777

---

# In 1.0.0

- first commit.
