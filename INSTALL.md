Build Instructions
------------------

# Requirements

- GNU make version 3.81 or later

This software will install on any operating system that implements POSIX.1-2008, available at [opengroup](http://pubs.opengroup.org/onlinepubs/9699919799/).


## Standard usage

`./configure && make && sudo make install` will work for most users.

Note: the man and html documentation pages will always be generated if *lowdown* is installed on your system. However, if you don't ask to build the documentation the final `DESTDIR` directory will do not contains any documentation at all.

## Customization

You can customize paths via flags given to configure. See `./configure --help` for a list of all available configure options.

## Runtime dependencies

- execline version 2.7.0.0 or later: http://skarnet.org/software/execline/
- s6 version 2.10.0.0 or later: http://skarnet.org/software/s6/
- s6-rc version 0.5.2.1 or later: http://skarnet.org/software/s6-rc/
- 66 version 0.6.0.0 or later: https://framagit.org/Obarun/66/
- 66-tools version 0.0.7.0 or later: https://framagit.org/Obarun/66-tools/
- s6-linux-utils version 2.5.1.4 or later: http://skarnet.org/software/s6-linux-utils/
- s6-portable-utils version 2.2.3.1 or later: http://skarnet.org/software/s6-portable-utils/
- bash
- iproute2
- kmod
- optional dependencies:
  * iptables,nftables,ebtables,arptables
  * dmraid
  * lvm2
  * btrfs-progs
  * zfs
