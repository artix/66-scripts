#!@BINDIR@/sh

## 66-yeller variable
export PROG="${MOD_NAME}"
export VERBOSITY="${MOD_VERBOSITY}"
export CLOCK_ENABLED=0
export COLOR_ENABLED="${MOD_COLOR}"

## script variable
service_dir="${MOD_MODULE_DIR}/service"
SV_REAL=

sv_boolean_list="CRYPTTAB SETUPCONSOLE FSTAB SWAP LVM LOCALE SYSUSER \
DMRAID BTRFS ZFS UDEV SYSCTL LOCAL CONTAINER TMPFILE MODULE_KERNEL \
MODULE_SYSTEM RANDOMSEED MNT_PROC MNT_SYS MNT_DEV MNT_RUN MNT_TMP CGROUPS \
MNT_PTS MNT_SHM MNT_NETFS POPULATE_SYS POPULATE_DEV POPULATE_RUN POPULATE_TMP"

sv_container_list="HARDWARECLOCK SETUPCONSOLE CRYPTTAB SWAP LVM DMRAID BTRFS \
ZFS UDEV UDEV_ADM SYSCTL FORCECHCK CGROUPS MODULE_SYSTEM RANDOMSEED MNT_NETFS"

die(){
  66-yeller -f "${@}"
  exit 111
}

check_empty_var(){
    name="${1}" var_value="${2}"
    if [ -z "${var_value}" ]; then
        die invalid value for variable: "${name}"
    fi
}

retrieve_sv_name(){
    sv=${1}
    case ${sv} in
        HARDWARECLOCK) SV_REAL="system-hwclock" ;;
        CRYPTTAB) SV_REAL="devices-crypttab" ;;
        SETUPCONSOLE) SV_REAL="system-fontnkey" ;;
        FSTAB) SV_REAL="mount-fstab" ;;
        SWAP) SV_REAL="mount-swap" ;;
        LVM) SV_REAL="devices-lvm" ;;
        DMRAID) SV_REAL="devices-dmraid" ;;
        BTRFS) SV_REAL="devices-btrfs" ;;
        ZFS) SV_REAL="devices-zfs" ;;
        UDEV) SV_REAL="udevd" ;;
        SYSCTL) SV_REAL="system-sysctl" ;;
        SYSUSER) SV_REAL="local-sysusers" ;;
        FORCECHCK) SV_REAL="system-fsck" ;;
        LOCAL) SV_REAL="local-rc" ;;
        TMPFILE) SV_REAL="local-tmpfiles" ;;
        MODULE_KERNEL) SV_REAL="modules-kernel" ;;
        MODULE_SYSTEM) SV_REAL="modules-system" ;;
        RANDOMSEED) SV_REAL="system-random" ;;
        MNT_PROC) SV_REAL="mount-proc" ;;
        MNT_SYS) SV_REAL="mount-sys" ;;
        MNT_DEV) SV_REAL="mount-dev" ;;
        MNT_RUN) SV_REAL="mount-run" ;;
        MNT_TMP) SV_REAL="mount-tmp" ;;
        CGROUPS) SV_REAL="mount-cgroups" ;;
        MNT_PTS) SV_REAL="mount-pts" ;;
        MNT_SHM) SV_REAL="mount-shm" ;;
        MNT_NETFS) SV_REAL="mount-netfs" ;;
        POPULATE_SYS) SV_REAL="populate-sys" ;;
        POPULATE_DEV) SV_REAL="populate-dev" ;;
        POPULATE_RUN) SV_REAL="populate-run" ;;
        POPULATE_TMP) SV_REAL="populate-tmp" ;;
        TTY) SV_REAL="tty-rc@" ;;
	LOCALE) SV_REAL="local-locale" ;;
        ## extra service not set by the environment section
        UDEV_ADM) SV_REAL="udevadm" ;;
    esac

    unset sv
}

sv_uncomment_list() {
    name=${1} list="$(find "${service_dir}" -mindepth 1 -type f)"

    retrieve_sv_name "${name}"
    66-yeller %benable%n service: "${SV_REAL}"
    for sv in ${list}; do
        sed -i "s:#*${SV_REAL}:${SV_REAL}:g" "${sv}" || die "unable to sed ${sv}"
    done

    unset list
}

sv_comment_list() {
    name=${1} list="$(find "${service_dir}" -mindepth 1 -type f)"
    file=
    retrieve_sv_name "${name}"
    66-yeller %rdisable%n service: "${SV_REAL}"
    for sv in ${list}; do
        sv_name=${sv##*/}
        if [ "${SV_REAL}" = "${sv_name}" ];then
            file="${sv}"
        fi
        sed -i "s:${SV_REAL}:#${SV_REAL}:g" "${sv}" || die "unable to sed ${sv}"
    done
    if [ -n "${file}" ];then
        rm -f "${file}" || die "unable to remove ${file}"
    fi
    unset list
}

sv_uncomment_real() {
    name=${1} list="$(find "${service_dir}" -mindepth 1 -type f)"
    66-yeller %benable%n service: "${name}"
    for sv in ${list}; do
        sed -i "s:#*${name}:${name}:g" "${sv}" || die "unable to sed ${sv}"
    done

    unset list
}

sv_comment_real() {
    name=${1} list="$(find "${service_dir}" -mindepth 1 -type f)"
    66-yeller %rdisable%n service: "${name}"
    file=
    for sv in ${list}; do
        sv_name=${sv##*/}
        if [ "${name}" = "${sv_name}" ];then
            file="${sv}"
        fi
        sed -i "s:${name}:#${name}:g" "${sv}" || die "unable to sed ${sv}"
    done
    if [ -n "${file}" ];then
        rm -f "${file}" || die "unable to remove ${file}"
    fi
}

66-yeller %benable%n service: "tty-earlier@tty12"
touch "${service_dir}/tty-earlier@tty12" || die "unable to create ${service_dir}/tty-earlier@tty12"

for sv in ${sv_boolean_list}; do
    if [ "${sv}" = "CONTAINER" ]; then
        continue
    fi
    eval val="\$${sv}"
    val="${val}"
    if [ "${val}" = "yes" ]; then
        sv_uncomment_list "${sv}"
    else
        sv_comment_list "${sv}"
    fi
done

comment_udev() {
    for sv in "udevd" "udevadm" "system-fontnkey" "devices-crypttab" \
            "devices-dmraid" "devices-btrfs" "devices-lvm" ; do
        sv_comment_real ${sv}
    done
}

comment_zfs() {
    for sv in "devices-zfs-import-cache" "devices-zfs-import-scan"; do
        sv_comment_real "${sv}"
    done
}

if [ "${UDEV}" = "no" ]; then
    comment_udev
fi

if [ "${CONTAINER}" = "yes" ]; then
    for sv in ${sv_container_list}; do
        sv_comment_list "${sv}"
    done
fi

if [ "${ZFS}" = "yes" ]; then
    if [ "${ZFS_IMPORT}" = "zpoolcache" ]; then
        sv_comment_real "devices-zfs-import-scan"
    else
    # set default to scan
        sv_comment_real "devices-zfs-import-cache"
    fi
else
    comment_zfs
fi

if [ "${LVM}" = "no" ] && \
[ "${DMRAID}" = "no" ] && \
[ "${BTRFS}" = "no" ] && \
[ "${ZFS}" = "no" ] && \
[ "${CRYPTTAB}" = "no" ]; then
    sv_comment_real "system-Devices"
fi

if [ "${TTY}" -gt 0 ]; then
    for i in $(seq "${TTY}"); do
        [ "${i}" -lt 12 ] || break
        66-yeller %benable%n service: "tty-rc@tty${i}"
        touch "${service_dir}/tty-rc@tty${i}" || die "unable to create ${service_dir}/tty-rc@tty${i}"
    done
fi

if execl-toc -X -V FIREWALL; then

    check_empty_var "FIREWALL" "${FIREWALL}"

    for sv in "iptables" "ip6tables" "nftables" "ebtables" "arptables"; do
        if [ "${FIREWALL}" = "${sv}" ]; then
            sv_uncomment_real "local-${sv}"
        else
            sv_comment_real "local-${sv}"
        fi
    done
else
    for sv in "local-iptables" "local-ip6tables" "local-nftables" "local-ebtables" "local-arptables"; do
        sv_comment_real "${sv}"
    done
fi

66-yeller "%bsuccessfully%n configured"
